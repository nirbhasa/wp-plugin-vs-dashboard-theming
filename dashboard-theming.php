<?php

/*
Plugin Name: VS Dashboard Theming
Description: CSS tweaks for the admin dashboard
Version: 1.0
*/

add_action('admin_head', 'vs_dashboard_admin_head');
add_action('admin_head-media-upload-popup', 'vs_dashboard_mediajs');


function vs_dashboard_admin_head() {
        echo '<link rel="stylesheet" type="text/css" href="' .plugins_url('dashboard.css', __FILE__). '">';
}

function vs_dashboard_mediajs() {

    global $post, $post_id;
   
?>
<script type="text/javascript" >
jQuery(document).ready(function($) {

  jQuery('#video-form a.describe-toggle-on').click(function () {
      
      jQuery('table.slidetoggle').removeClass('startclosed');
      jQuery(this).hide(); 
      jQuery('#video-form a.describe-toggle-off').show();  
  }); 
  
  jQuery('#video-form a.describe-toggle-off').click(function () {
     jQuery('table.slidetoggle').addClass('startclosed'); 
     jQuery(this).hide();
     jQuery('#video-form a.describe-toggle-on').show();
  });
       
});
</script>
<?php
}




?>